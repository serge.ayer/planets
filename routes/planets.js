const express = require('express');
const router = express.Router();

// Require controller modules
const planetsController = require('../controllers/planets_controller');

router.get('/:planetId', planetsController.onePlanet);
// GET all planets at root
router.get('/', planetsController.allPlanets);

module.exports = router;
