// return the all planets responses
exports.allPlanets = (req, res) => {
  const planets = [
    {"name": "Mercury", "position": 1},
    {"name": "Venus", "position": 2},
    {"name": "Earth", "position": 3},
    {"name": "Mars", "position": 4},
    {"name": "Jupiter", "position": 5},
    {"name": "Saturn", "position": 6},
    {"name": "Uranus", "position": 7},
    {"name": "Neptune", "position": 8},
  ];
  res.send(planets);
};

exports.onePlanet = (req, res) => {
  const detailedPlanetList = [
    {"name": "Mercury", "position": 1, "confirmed_moons":0, "provisional_moons":0},
    {"name": "Venus", "position": 2, "confirmed_moons":0, "provisional_moons":0},
    {"name": "Earth", "position": 3, "confirmed_moons":1, "provisional_moons":0},
    {"name": "Mars", "position": 4, "confirmed_moons":2, "provisional_moons":0},
    {"name": "Jupiter", "position": 5, "confirmed_moons":53, "provisional_moons":26},
    {"name": "Saturn", "position": 6, "confirmed_moons":53, "provisional_moons":29},
    {"name": "Uranus", "position": 7, "confirmed_moons":27, "provisional_moons":0},
    {"name": "Neptune", "position": 8, "confirmed_moons":14, "provisional_moons":0},
  ];
  if (req.params.planetId < 1 || req.params.planetId > detailedPlanetList.length) {
    let error = new Error();
    error.statusCode = 404;
    error.statusMessage = "Invalid planet id";
    throw error;
  }
  else {
    res.send(detailedPlanetList[req.params.planetId - 1]);
  }
};
